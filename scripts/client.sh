#!/bin/sh
for i in $(seq 50)
do
  netperf  -l 60 -H 192.168.10.144 -t TCP_RR -- -r 100,200
done
for i in $(seq 50)
do
  netperf  -l 60 -H 192.168.10.144 -t UDP_RR -- -r 100,200
done
for i in $(seq 50)
do
   iperf3 -i 2 -f M -c 192.168.10.144 -t 20
done
for i in $(seq 50)
do
   perf stat -d nuttcp -t 192.168.10.144
done
for i in $(seq 50)
do
   perf stat -d nuttcp -r 192.168.10.144
done
for i in $(seq 50)
do
   /usr/bin/linpack
done
for i in $(seq 50)
do
   /usr/bin/nbench
done
usr/lib/script/config-run
usr/lib/script/lmbench configuration
